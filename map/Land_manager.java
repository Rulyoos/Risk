package map;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import back.Unites;
import back.Units_manager;
import players_manager.Player;

public class Land_manager {

	protected List<Territoires> LandList = new ArrayList<>();
	protected List<Region> ZoneList = new ArrayList<>();

	public List<Region> getZoneList() {
		return ZoneList;
	}

	public List<Territoires> getLandList() {
		return LandList;
	}

	public Land_manager(String map) {

		// to switch between earth and maybe another map
		switch (map) {
		case "Terre":
			Build_Terre();
			Build_Voisins();
			break;

		default:
			break;
		}
	}

	// to build the map
	protected void Build_Terre() {
		Region Zone = null;
		FileReader fr = null;
		try {
			fr = new FileReader("src/map/earth");
			BufferedReader br = new BufferedReader(fr);
			String sCurrentLine;
			
			String Region = "";

			try {
				while ((sCurrentLine = br.readLine()).isEmpty() == false) {
					if (sCurrentLine.startsWith("-")) {
						Region = sCurrentLine.substring(1, sCurrentLine.length());
						Zone = new Region(Region);
						ZoneList.add(Zone);
					} else {
						String x = sCurrentLine.split("/")[1];
						int X = Integer.parseInt(x);
						String y = sCurrentLine.split("/")[2];
						int Y = Integer.parseInt(y);
						String country = sCurrentLine.split("/")[0];
						Territoires l = new Territoires(country, Region);
						l.setPosition(X, Y);
						Zone.AddCountry(l);
						LandList.add(l);

					}

				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Territoires getByName(String Country) {
		for (int i = 0; i < this.LandList.size(); i++) {
			if (this.LandList.get(i).getName().equals(Country)) {
				return this.LandList.get(i);
			}
		}
		return null;
	}

	public List<Territoires> getByPlayer(Player player) {
		List<Territoires> PLand = new ArrayList<>();
		for (int i = 0; i < this.LandList.size(); i++) {
			if (this.LandList.get(i).getOwner().equals(player)) {
				PLand.add(this.LandList.get(i));
			}
		}
		return PLand;
	}

	protected void Build_Voisins() {
		FileReader fr = null;
		try {
			fr = new FileReader("src/map/earthLinks");

			BufferedReader br = new BufferedReader(fr);
			String sCurrentLine;
			String Country1 = "";
			String Country2 = "";

			try {
				while ((sCurrentLine = br.readLine()).isEmpty() == false) {
					Country1 = sCurrentLine.split("-")[0];
					Country2 = sCurrentLine.split("-")[1];
					getByName(Country1).AddVoisins(getByName(Country2));
					getByName(Country2).AddVoisins(getByName(Country1));
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	private void Fight(Units_manager A, Units_manager B, Unites unitAtt) {// makes 2 unit fight
		Unites unitDef = B.getTotUnites().get(0);
		int min_Def_prio = 101;
		for (int i = 0; i < B.getTotUnites().size(); i++) {
			if (B.getTotUnites().get(i).getPrio_DEF() < min_Def_prio) {
				min_Def_prio = B.getTotUnites().get(i).getPrio_DEF();
				unitDef = B.getTotUnites().get(i);
			}
		}
		int minDef = unitDef.getPuissance()[0];
		int maxDef = unitDef.getPuissance()[1];
		int minAtt = unitAtt.getPuissance()[0];
		int maxAtt = unitAtt.getPuissance()[1];
		int DefPower = minDef + (int) (Math.random() * ((maxDef - minDef) + 1));
		int AttPower = minAtt + (int) (Math.random() * ((maxAtt - minAtt) + 1));

		if (DefPower >= AttPower) {
			if(unitAtt.getName().equals("Soldat")) {
				A.remove_Soldats();
			}
			if(unitAtt.getName().equals("Cavalier")) {
				A.remove_Cavaliers();
			}
			if(unitAtt.getName().equals("Canon")) {
				A.remove_Canons();
			}
			// System.out.println("Successful defense");
		} else {
			if(unitDef.getName().equals("Soldat")) {
				B.remove_Soldats();
			}
			if(unitDef.getName().equals("Cavalier")) {
				B.remove_Cavaliers();
			}
			if(unitDef.getName().equals("Canon")) {
				B.remove_Canons();
			}
			// System.out.println("Defense got crushed");
		}

	}

	public void MoveUnit(Territoires A,Territoires B,String Unit) {
		if(A.getCommander().getTotUnites().size()>1) {
		switch(Unit) {
		case "Soldat":
			if(A.getCommander().getSoldats().size()>0) {
			A.getCommander().remove_Soldats();
			B.getCommander().add_Soldats(1);
			}
			break;
		case "Cavalier":
			if(A.getCommander().getCavaliers().size()>0) {
			A.getCommander().remove_Cavaliers();
			B.getCommander().add_Cavaliers(1);
			}
			break;
		case "Canon":
			if(A.getCommander().getCanons().size()>0) {
			A.getCommander().remove_Canons();
			B.getCommander().add_Canons(1);
			}
			break;
		}
		}
		
	}

	public void AfficherVoisins(Territoires Land) {
		for (int i = 0; i < Land.getVoisins().size(); i++) {
			System.out.println(
					Land.getVoisins().get(i).getName() + " player " + Land.getVoisins().get(i).getOwner().getId());
		}
	}

	public void AttackinNb(Territoires A, Territoires B, Unites Unit, int n) {
		for (int i = 0; i < n; i++) {
			Attack(A, B, Unit);
		}
	}

	public boolean Attack(Territoires A, Territoires B, Unites Unit) {// A attacks B

		if (A.getCommander().getTotUnites().size() > 1) {
			A.getCommander();
			B.getCommander();
			Fight(A.getCommander(), B.getCommander(), Unit);
			if (B.getCommander().getTotUnites().size() == 0) {// A wins and B has no unit left
				B.setOwner(A.getOwner());

				// System.out.println("Country "+B.getName()+" now belong to player "+
				// A.getOwner().getId());
				
				if (Unit.getName().equals("Soldat")) {// still needs to manage deplacement points
					A.getCommander().remove_Soldats();
					B.getCommander().add_Soldats(1);
				} else if (Unit.getName().equals("Cavalier")) {
					A.getCommander().remove_Cavaliers();
					B.getCommander().add_Cavaliers(1);
				} else if (Unit.getName().equals("Canons")) {
					A.getCommander().remove_Canons();
					B.getCommander().add_Canons(1);
				}
				return true;
			}
		} else {
			return false;
		}
		return false;

	}
}

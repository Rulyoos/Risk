package map;

import java.util.*;

public class Region {
	private List<Territoires> Countries;
	public List<Territoires> getCountries() {
		return Countries;
	}

	public String getName() {
		return Name;
	}

	private String Name;
	
	
	public Region(String Name) {
		this.Name=Name;
		Countries=new ArrayList<>();
	}
	
	public void AddCountry(Territoires Land) {
		this.Countries.add(Land);
	}
}

package map;

import java.util.*;

import back.Units_manager;
import players_manager.Player;

public class Territoires {
	protected Player owner=null;
	protected String Name;
	protected String Region;
	protected List<Territoires> voisins;
	protected Units_manager Commander;
	protected int[] position=new int[2];//[x,y]

	public int[] getPosition() {
		return position;
	}

	public void setPosition(int x,int y) {
		this.position[0]=x;
		this.position[1]=y;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getRegion() {
		return Region;
	}

	public void setRegion(String region) {
		Region = region;
	}

	public List<Territoires> getVoisins() {
		return voisins;
	}

	public void setVoisins(List<Territoires> voisins) {
		this.voisins = voisins;
	}

	public Units_manager getCommander() {
		return Commander;
	}

	public void setCommander(Units_manager commander) {
		Commander = commander;
	}
	public Territoires(String nom,String Region) {
		this.Name = nom;
		this.Region=Region;
		this.voisins = new ArrayList<>();
		this.Commander=new Units_manager();

	}

	protected void switch_player(Player player) {
		this.owner = player;
	}
	
	protected void AddVoisins(Territoires voisin) {
		this.voisins.add(voisin);
	}



	public boolean isVoisins(Territoires B) {
		for(int i=0;i<this.getVoisins().size();i++) {
			if(this.getVoisins().get(i).equals(B)) {
				return true;
			}
		}
		return false;
	}

}

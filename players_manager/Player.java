package players_manager;


import java.util.Random;

import back.Canons;
import back.Cavaliers;

import map.Land_manager;
import map.Region;
import map.Territoires;


public class Player {

	protected String color;
	protected int armees = 0;
	protected int Cavaliers = 0;
	protected int Canons = 0;
	protected int id = 0;
	protected String phase = "null";

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getCavaliers() {
		return Cavaliers;
	}

	public int getCanons() {
		return Canons;
	}

	public void setArmees(int armees) {
		this.armees = armees;
	}

	public void setCavaliers(int cavaliers) {
		Cavaliers = cavaliers;
	}

	public void setCanons(int canons) {
		Canons = canons;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getArmees() {
		return armees;
	}

	public int getId() {
		return id;
	}

	public Player(int num) {
		this.id = num;
	}

	public int getNbCountries(Land_manager LM) {
		int compteur = 0;
		for (int i = 0; i < LM.getLandList().size(); i++) {
			if (LM.getLandList().get(i).getOwner() == this) {
				compteur += 1;
			}
		}
		return compteur;
	}

	private Region CheckRegion(Land_manager LM) {
		boolean zoneOwner = true;
		
		for (int i = 0; i < LM.getZoneList().size(); i++) {

			if (zoneOwner == true && i >= 1) {
				return LM.getZoneList().get(i - 1);
			} else {
				zoneOwner = true;
			}
			for (int k = 0; k < LM.getZoneList().get(i).getCountries().size(); k++) {
				if (LM.getZoneList().get(i).getCountries().get(k).getOwner() != this) {
					zoneOwner = false;
				}
			}
		}

		return null;
	}

	private int RenfortByRegion(Land_manager LM) {
		Region Zone = CheckRegion(LM);
		if (Zone != null) {
			return Zone.getCountries().size() / 3;
		} else {
			return 0;
		}
	}

	public void GetRenfort(Land_manager LM) {
		int nbTerr = this.getNbCountries(LM);
		int Renforts = nbTerr / 3;// apport en fx de territoires
		Renforts += 1 * random(1);// 50% de chance de recevoir une arm�e de plus
		Renforts += RenfortByRegion(LM);// renforts par r�gion

		int Arm = this.getArmees();
		Arm += Renforts;
		this.setArmees(Arm);
		System.out.println("Renforts:" + this.getArmees());

	}

	public void PlaceUnit(String unit, Territoires A) {
		if (A != null) {
			if (this.equals(A.getOwner())) {

				switch (unit) {
				case "Soldat":
					if (this.armees > 0) {
						A.getCommander().add_Soldats(1);
						this.armees = this.armees - 1;
					}
					break;
				case "Cavalier":
					if (this.Cavaliers > 0) {
						A.getCommander().add_Cavaliers(1);
						this.Cavaliers = this.Cavaliers - 1;
					}
					break;
				case "Canon":
					if (this.Canons > 0) {
						A.getCommander().add_Canons(1);;
						this.Canons = this.Canons - 1;
					}
					break;
				default:
					break;
				}
			}

		}
		if (this.armees == 0 && this.Cavaliers == 0 && this.Canons == 0) {
			this.setPhase("Second");
		}
	}

	public void CreateCavaliers() {
		back.Cavaliers cav = new Cavaliers();
		if (this.getArmees() >= cav.getCout()) {
			armees -= cav.getCout();
			Cavaliers += 1;
		}
	}

	public void CreateCanons() {
		back.Canons can = new Canons();
		if (this.getArmees() >= can.getCout()) {
			armees -= can.getCout();
			Canons += 1;
		}
	}

	public void MoveUnit() {
	};

	protected int random(int b) {
		Random rand = new Random();
		return rand.nextInt(b);
	}

}

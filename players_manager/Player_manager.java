package players_manager;

import java.util.*;

public class Player_manager {
	protected String[] colors= {"red","blue","orange","green","yellow","violet"};
	protected List<Player> PlayerList=new ArrayList<>();
	
	
	public Player getTour() {//savoir de quel joueur c'est le tour
		for(int i=0;i<this.PlayerList.size();i++) {
			if(PlayerList.get(i).getPhase().equals("First") || PlayerList.get(i).getPhase().equals("Second")) {
				return PlayerList.get(i);
			}
		}
		return null;
	}
	public List<Player> getPlayerList() {
		return PlayerList;
	}
	public Player_manager(int PlayerNb) {
		for(int i=0;i<PlayerNb;i++) {
			this.Add_player(new Player(i));
			this.getPlayerList().get(i).setColor(colors[i]);
		}
		
	}
	public void Add_player(Player player) {
		this.PlayerList.add(player);
	}
	
	public void Remove_player(Player player) {
		this.PlayerList.remove(player);
	}
}

package back;

public abstract class Unites {
	protected int Prio_ATT;
	protected int Prio_DEF;
	protected int Cout;
	protected int Movement;
	protected int[] Puissance=new int[2];
	protected String Name;
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getPrio_ATT() {
		return Prio_ATT;
	}
	public void setPrio_ATT(int prio_ATT) {
		Prio_ATT = prio_ATT;
	}
	public int getPrio_DEF() {
		return Prio_DEF;
	}
	public void setPrio_DEF(int prio_DEF) {
		Prio_DEF = prio_DEF;
	}
	public int getCout() {
		return Cout;
	}
	public void setCout(int cout) {
		Cout = cout;
	}
	public int getMovement() {
		return Movement;
	}
	public void setMovement(int movement) {
		Movement = movement;
	}
	public int[] getPuissance() {
		return Puissance;
	}
	public void setPuissance(int[] puissance) {
		Puissance = puissance;
	}
	
	
	

}

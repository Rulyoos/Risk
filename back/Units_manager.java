package back;

import java.util.*;

public class Units_manager {
	protected List<Canons> Canons=new ArrayList<>();
	protected List<Soldats> Soldats=new ArrayList<>();
	protected List<Cavaliers> Cavaliers=new ArrayList<>();
	protected List<Unites> TotUnites=new ArrayList<>();

	public List<Canons> getCanons() {
		return Canons;
	}

	public List<Soldats> getSoldats() {
		return Soldats;
	}

	public List<Cavaliers> getCavaliers() {
		return Cavaliers;
	}

	public List<Unites> getTotUnites() {
		return TotUnites;
	}

	public Units_manager() {
		
		this.add_Soldats(1);
	}

	public void add_Unit(int nb) {
		for (int k = 0; k < nb; k++) {
			TotUnites.add(new Soldats());
		}
	}

	public void add_Unit(Unites unit) {
		TotUnites.add(unit);
	}

	public void Remove_Unit(int i) {
		TotUnites.remove(i);
	}

	public void add_Soldats(int num) {
		for (int i = 0; i < num; i++) {
			Soldats s = new Soldats();
			Soldats.add(s);
			this.add_Unit(s);
		}
	}

	public void remove_Soldats() {
		Soldats.remove(Soldats.size() - 1);
		int i = TotUnites.size() - 1;
		while (TotUnites.get(i).getName() != "Soldat" && i>=0) {
			i--;
		}

		this.Remove_Unit(i);

	}

	public void add_Cavaliers(int num) {
		for (int i = 0; i < num; i++) {
			Cavaliers c = new Cavaliers();
			Cavaliers.add(c);
			this.add_Unit(c);
		}
	}

	public void remove_Cavaliers() {
		Cavaliers.remove(Cavaliers.size() - 1);
		int i = TotUnites.size() - 1;
		while (TotUnites.get(i).getName() != "Cavalier" && i>=0) {
			i--;
		}

		this.Remove_Unit(i);
	}

	public void add_Canons(int num) {
		for (int i = 0; i < num; i++) {
			Canons s = new Canons();
			Canons.add(s);
			this.add_Unit(s);
		}
	}

	public void remove_Canons() {
		Canons.remove(Soldats.size() - 1);
		int i = TotUnites.size() - 1;
		while (TotUnites.get(i).getName() != "Canon" && i>=0) {
			i--;
		}

		this.Remove_Unit(i);
	}

}

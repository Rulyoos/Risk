package Fenetre_jeu;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;


public class Audio {
	
	private Clip clip;
	public static Audio s;
	
//constructeur
public Audio(String file) {
		
	try {
	         // Open an audio input stream.
			File soundFile = new File(file);
	         AudioInputStream audioIn = AudioSystem.getAudioInputStream(soundFile);
	         // Get a sound clip resource.
	         clip = AudioSystem.getClip();
	         // Open audio clip and load samples from the audio input stream.
	         clip.open(audioIn);
	          
	      } catch (UnsupportedAudioFileException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      } catch (LineUnavailableException e) {
	         e.printStackTrace();
	      }
}
//getters
public Clip getClip() {return clip;}
//methodes
public void play() {
	clip.start();
}
public void stop() {
	clip.stop();
}
public void loop() {
    clip.loop(Clip.LOOP_CONTINUOUSLY);  // repeat forever
}

public static void playSound(String son,Boolean loop) {
	s=new Audio(son);
	s.play();
	if(loop==true) {
		s.loop();
	}

}

public static void StopSound() {
	s.stop();
}
}
package Fenetre_jeu;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

import players_manager.Player;


@SuppressWarnings("serial")
public class Label extends JLabel{
	private String nom;
	public Label(int x,int y,String name) {
		this.setNom(name);
		this.setForeground(Color.BLACK);
		this.setSize(50, 50);
		this.setLocation(x, y);
		this.setFont(new Font("Arial",15,15));
		this.setVisible(true);
		
		
		/*
		ImageIcon IcoFond = new ImageIcon(getClass().getResource(Path));
		this.setSize(IcoFond.getIconWidth(), IcoFond.getIconHeight());
		this.setIcon(IcoFond);
	*/
	
	}
	
	public void setText(Player player) {
		switch(this.getNom()) {
		case "Soldats":
			this.setText(player.getArmees()+"");
			this.repaint();
			break;
		case "Cavaliers":
			this.setText(player.getCavaliers()+"");
			this.repaint();
			break;
		case "Canons":
			this.setText(""+player.getCanons()+"");
			this.repaint();
			break;
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	

}

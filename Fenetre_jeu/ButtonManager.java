package Fenetre_jeu;

import java.awt.Container;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;

import javax.swing.JPanel;

import back.Canons;
import back.Cavaliers;
import back.Soldats;
import game.Game_Manager;
import map.Land_manager;
import players_manager.Player;

public class ButtonManager {

	public static List<Button> Second = new ArrayList<>();
	public static List<Button> Select = new ArrayList<>();
	public static List<Button> All = new ArrayList<>();

	public static Player getPlayer() {
		int k = 0;
		Player player = null;
		do {
			player = All.get(k).getLand().getOwner();
		} while (player.getPhase().equals("null"));

		return player;

	}

	public static void PlaceUnitButtons(JPanel pan) {
		KInter ML = new KInter();
		Button Soldat = new Button("/images/soldat.png", 3);// 3 pour les soldats
		Button Cavalier = new Button("/images/cavalier.png", 4);// 4 pour les cavaliers
		Button Canon = new Button("/images/canon.png", 5);// 5 pour les canons

		Select.add(Soldat);
		Select.add(Cavalier);
		Select.add(Canon);

		Soldat.addMouseListener(ML);
		Cavalier.addMouseListener(ML);
		Canon.addMouseListener(ML);

		pan.add(Soldat);
		pan.add(Cavalier);
		pan.add(Canon);

		Soldat.setLocation(50, 500);
		Cavalier.setLocation(50, 560);
		Canon.setLocation(50, 620);

		Button but = new Button("/images/ajout.png", 1);// 1 pour l'ajout de Cavaliers
		but.addMouseListener(ML);
		pan.add(but);
		but.setLocation(100, 560);
		Button but2 = new Button("/images/ajout.png", 2);// 2 pour l'ajout de Canons
		but2.addMouseListener(ML);
		pan.add(but2);
		but2.setLocation(100, 620);
		pan.repaint();
		;

	}

	public static void PlacePlayerButton(Player player, JPanel pan, Land_manager LM) {
		String color = player.getColor();

		if (color.equals("blue")) {
			setup_Buttons(player, pan, LM, "/images/blue_player50.png", All);
		} else if (color.equals("red")) {
			setup_Buttons(player, pan, LM, "/images/red_player50.png", All);
		} else if (color.equals("orange")) {
			setup_Buttons(player, pan, LM, "/images/orange_player50.png", All);
		} else if (color.equals("green")) {
			setup_Buttons(player, pan, LM, "/images/green_player50.png", All);
		} else if (color.equals("violet")) {
			setup_Buttons(player, pan, LM, "/images/violet_player50.png", All);
		} else if (color.equals("yellow")) {
			setup_Buttons(player, pan, LM, "/images/yellow_player50.png", All);

		}
	}

	public static void RemoveSecondButtons(Container container) {
		for (int i = 0; i < Second.size(); i++) {
			container.remove(Second.get(i));
		}
		Second = new ArrayList<>();
	}

	public static void AddSecondButtons(Container container, Button but, Player player) {
		KInter K = new KInter();
		ButtonManager.RemoveSecondButtons(but.getParent());
		Button but2 = null;
		for (int i = 0; i < but.getLand().getVoisins().size(); i++) {
			if (but.getLand().getVoisins().get(i).getOwner().equals(player)) {
				but2 = new Button(but.getLand().getVoisins().get(i), 7, "green", but.getLand());// 7 move
			} else {
				but2 = new Button(but.getLand().getVoisins().get(i), 6, "red", but.getLand());// 6 attack
			}

			but2.setLocation(1100, 400 + i * 40);
			but2.addMouseListener(K);
			Second.add(but2);
			container.add(but2);
			container.repaint();
		}

	}

	public static void ToolTip(Button but) {
		but.setToolTipText(but.getLand().getName() + " " + "Soldats=" + but.getLand().getCommander().getSoldats().size()
				+ " Cavaliers=" + but.getLand().getCommander().getCavaliers().size() + " Canons="
				+ but.getLand().getCommander().getCanons().size()

		);
	}

	public static void setAllVisble() {
		for (int i = 0; i < All.size(); i++) {
			All.get(i).setVisible(true);
			All.get(i).repaint();

		}
	}

	public static void SetOnlyVoisinVisible(Button but) {
		for (int i = 0; i < All.size(); i++) {
			All.get(i).setVisible(false);
		}
		for (int i = 0; i < but.getLand().getVoisins().size(); i++) {
			for (int k = 0; k < All.size(); k++) {

				if (but.getLand().getVoisins().get(i).equals(All.get(k).getLand())) {
					All.get(k).setVisible(true);
					All.get(k).repaint();

				}

			}

		}
		but.setVisible(true);
		but.repaint();

	}

	private static void setup_Buttons(Player player, JPanel pan, Land_manager LM, String Path, List<Button> colorList) {
		KInter ML = new KInter();
		for (int i = 0; i < LM.getByPlayer(player).size(); i++) {
			Button but = new Button(Path, LM.getByPlayer(player).get(i), 0);
			but.addMouseListener(ML);
			pan.add(but);
			ToolTip(but);
			but.setBounds(LM.getByPlayer(player).get(i).getPosition()[0],
					LM.getByPlayer(player).get(i).getPosition()[1], 50, 50);
			pan.repaint();
			colorList.add(but);

		}

	}

}

class KInter implements MouseListener {
	private Button but;
	private Player joueur;

	public KInter() {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		joueur = Game_Manager.GetPlayer();
		but = (Button) arg0.getComponent();
		switch (but.getNb()) {

		case 0:// selection des territoires
			switch (joueur.getPhase()) {
			case "First":
				if (ButtonManager.Select.get(0).getState().equals("ON")) {
					joueur.PlaceUnit("Soldat", but.getLand());
					ButtonManager.ToolTip(but);
					LabelManager.LabelPlayer(joueur);

				} else if (ButtonManager.Select.get(1).getState().equals("ON")) {
					joueur.PlaceUnit("Cavalier", but.getLand());
					ButtonManager.ToolTip(but);
					LabelManager.LabelPlayer(joueur);
				} else if (ButtonManager.Select.get(2).getState().equals("ON")) {
					joueur.PlaceUnit("Canon", but.getLand());
					ButtonManager.ToolTip(but);
					LabelManager.LabelPlayer(joueur);
				}

				LabelManager.setText2(joueur);
				break;
			case "Second":
				if (but.getLand().getOwner().equals(joueur)) {
					ButtonManager.AddSecondButtons(but.getParent(), but, joueur);
				}
			}

			break;
		case 1:// boutons d'ajout de cavalier
			joueur.CreateCavaliers();
			LabelManager.LabelPlayer(joueur);

			break;
		case 2:// boutons d'ajout de canons
			joueur = Game_Manager.GetPlayer();
			joueur.CreateCanons();
			LabelManager.LabelPlayer(joueur);

			break;
		case 3:// selection des soldats
			ButtonManager.Select.get(0).SwapStateON("/images/soldatON.png");
			ButtonManager.Select.get(1).SwapStateOFF("/images/cavalier.png");
			ButtonManager.Select.get(2).SwapStateOFF("/images/canon.png");
			break;
		case 4:// selection des cavaliers
			ButtonManager.Select.get(0).SwapStateOFF("/images/soldat.png");
			ButtonManager.Select.get(1).SwapStateON("/images/cavalierON.png");
			ButtonManager.Select.get(2).SwapStateOFF("/images/canon.png");
			break;
		case 5:// selection des canons
			ButtonManager.Select.get(0).SwapStateOFF("/images/soldat.png");
			ButtonManager.Select.get(1).SwapStateOFF("/images/cavalier.png");
			ButtonManager.Select.get(2).SwapStateON("/images/canonON.png");
			break;
		case 6:
			boolean q = false;
			if (joueur.getPhase().equals("Second")) {
				if (ButtonManager.Select.get(2).getState().equals("ON")) {
					q = Game_Manager.getLM().Attack(but.getOrigine(), but.getLand(), new Canons());
					
				} else if (ButtonManager.Select.get(1).getState().equals("ON")) {
					q = Game_Manager.getLM().Attack(but.getOrigine(), but.getLand(), new Cavaliers());
				} else {
					q = Game_Manager.getLM().Attack(but.getOrigine(), but.getLand(), new Soldats());
				}

				if (q) {
					for (int i = 0; i < ButtonManager.All.size(); i++) {
						if (but.getLand().equals(ButtonManager.All.get(i).getLand())) {
							ButtonManager.All.get(i).RepaintConquest(joueur.getColor());
							ButtonManager.RemoveSecondButtons(but.getParent());
						}
					}
				}
				if(Game_Manager.Victory()!=null) {
					Container container=but.getParent();
					but.getParent().removeAll();
					Audio.StopSound();
					container.add(new EndPan(Game_Manager.Victory().getColor()));
				}

			}
			for (int i = 0; i < ButtonManager.All.size(); i++) {
				ButtonManager.ToolTip(ButtonManager.All.get(i));
			}
			break;
		case 7:
			if (ButtonManager.Select.get(1).getState().equals("ON")) {
				Game_Manager.getLM().MoveUnit(but.getOrigine(), but.getLand(), "Cavalier");
			} else if (ButtonManager.Select.get(2).getState().equals("ON")) {
				Game_Manager.getLM().MoveUnit(but.getOrigine(), but.getLand(), "Canon");
			} else {
				Game_Manager.getLM().MoveUnit(but.getOrigine(), but.getLand(), "Soldat");
			}
			for (int i = 0; i < ButtonManager.All.size(); i++) {
				ButtonManager.ToolTip(ButtonManager.All.get(i));
			}
			break;
		}

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

		but = (Button) arg0.getComponent();
		if (but.getNb() == 0) {
			ButtonManager.SetOnlyVoisinVisible(but);

		}

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		ButtonManager.setAllVisble();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

}

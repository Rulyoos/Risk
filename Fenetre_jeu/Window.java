package Fenetre_jeu;

import javax.swing.JFrame;



@SuppressWarnings("serial")
public class Window extends JFrame{
	
	public Window() {
		this.setTitle("Risk");
		this.setSize(1250,750);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setContentPane(new Menu_Pan());
		//this.setContentPane(new EndPan("blue"));
		this.setResizable(false);
		this.setVisible(true);
	}

}

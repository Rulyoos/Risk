package Fenetre_jeu;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import map.Territoires;

@SuppressWarnings("serial")
public class Button extends JButton {
	private ImageIcon IcoFond;
	private int nb;
	private Territoires land;
	private Territoires Origine;
	private String State="off";
	

	public Territoires getLand() {
		return land;
	}

	public void setLand(Territoires land) {
		this.land = land;
	}

	public int getNb() {
		return nb;
	}

	public Button(Territoires land,int nbu,String color,Territoires Origine) {
		this.setLayout(null);
		this.setOrigine(Origine);
		this.setLand(land);
		this.nb=nbu;
		if(color.equals("red")) {
			this.setBackground(Color.red);
		}else if(color.equals("green")) {
			this.setBackground(Color.GREEN);
		}
		
		this.setSize(150,40);
		this.setText(land.getName());
	}
	
	public Button(String Path) {

		this.setLayout(null);
	
		IcoFond = new ImageIcon(getClass().getResource(Path));
		this.setSize(IcoFond.getIconWidth(), IcoFond.getIconHeight());
		this.setIcon(IcoFond);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);

	}

	public Button(String Path,Territoires land,int nbu) {

		this.setLayout(null);
		this.nb=nbu;
		this.setLand(land);
		IcoFond = new ImageIcon(getClass().getResource(Path));
		this.setSize(IcoFond.getIconWidth(), IcoFond.getIconHeight());
		this.setIcon(IcoFond);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);

	}

	public Button(String Path, int nb) {

		this.setLayout(null);
		this.nb = nb;
		IcoFond = new ImageIcon(getClass().getResource(Path));
		this.setSize(IcoFond.getIconWidth(), IcoFond.getIconHeight());
		this.setIcon(IcoFond);
		this.setContentAreaFilled(false);
		this.setBorderPainted(false);

	}
	
	public void SwapStateON(String Path) {
		this.setState("ON");
		IcoFond = new ImageIcon(getClass().getResource(Path));
		this.setSize(IcoFond.getIconWidth(), IcoFond.getIconHeight());
		this.setIcon(IcoFond);
		this.repaint();
	}
	public void SwapStateOFF(String Path) {
		this.setState("OFF");
		IcoFond = new ImageIcon(getClass().getResource(Path));
		this.setSize(IcoFond.getIconWidth(), IcoFond.getIconHeight());
		this.setIcon(IcoFond);
		this.repaint();
	}
	
	private void Repaint(String Path) {
		
		IcoFond = new ImageIcon(getClass().getResource(Path));
		//this.setSize(IcoFond.getIconWidth(), IcoFond.getIconHeight());
		this.setIcon(IcoFond);
		this.repaint();
	}
	public void RepaintConquest(String color) {
		switch(color) {
		case "red":
			this.Repaint("/images/red_player50.png");
			break;
		case "blue":
			this.Repaint("/images/blue_player50.png");
			break;
		case "yellow":
			this.Repaint("/images/yellow_player50.png");
			break;
		case "violet":
			this.Repaint("/images/violet_player50.png");
			break;
		case "green":
			this.Repaint("/images/green_player50.png");
			break;
		case "orange":
			this.Repaint("/images/orange_player50.png");
			break;
		}
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public Territoires getOrigine() {
		return Origine;
	}

	public void setOrigine(Territoires origine2) {
		Origine = origine2;
	}



}

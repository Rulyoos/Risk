package Fenetre_jeu;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class EndPan extends JPanel{
	private ImageIcon IcoFond;
	private Image FondMenu;
	public EndPan(String color) {
		Audio.playSound("src/music/witnessed.wav", true);
		Label l=new Label(600,400,"");
		l.setSize(300,300);
		switch(color) {
		case "red":
			IcoFond = new ImageIcon(getClass().getResource("/images/MenuFD2.png"));
			this.FondMenu = this.IcoFond.getImage();
			this.add(l);
			l.setText("Red Wins");
			this.repaint();
			
			break;
		case "blue":
			IcoFond = new ImageIcon(getClass().getResource("/images/MenuFD2.png"));
			this.FondMenu = this.IcoFond.getImage();
			this.add(l);
			l.setText("Blue Wins");
			this.repaint();
			break;
		case "green":
			IcoFond = new ImageIcon(getClass().getResource("/images/MenuFD2.png"));
			this.FondMenu = this.IcoFond.getImage();
			this.add(l);
			l.setText("Green Wins");
			this.repaint();
			break;
		case "yellow":
			IcoFond = new ImageIcon(getClass().getResource("/images/MenuFD2.png"));
			this.FondMenu = this.IcoFond.getImage();
			this.add(l);
			l.setText("Yellow Wins");
			this.repaint();
			break;
		case "violet":
			IcoFond = new ImageIcon(getClass().getResource("/images/MenuFD2.png"));
			this.FondMenu = this.IcoFond.getImage();
			this.add(l);
			l.setText("Violet Wins");
			this.repaint();
			break;
		case "orange":
			IcoFond = new ImageIcon(getClass().getResource("/images/MenuFD2.png"));
			this.FondMenu = this.IcoFond.getImage();
			this.add(l);
			l.setText("Orange Wins");
			this.repaint();
			break;
		
		}
		
		
		
	}
	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics g2 = (Graphics2D) g;
		g2.drawImage(this.FondMenu, 0, 0, null);

	}
}

package Fenetre_jeu;


import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import Fenetre_jeu.Audio;
import Fenetre_jeu.Checkbox;
import game.Game_Manager;
import players_manager.Player;


@SuppressWarnings("serial")
public class Menu_Pan extends JPanel {
	
	private Game_Manager GM;
	private ImageIcon IcoFond;
	private Image FondMenu;
	private Button jouer;
	public  Checkbox music;
	private String actual;

	public Game_Manager getGM() {
		return GM;
	}

	public Menu_Pan() {
		this.setLayout(null);
		//Music
		actual="src/music/ForValour.wav";
		Audio.playSound(actual, true);
		//BackGround
		IcoFond = new ImageIcon(getClass().getResource("/images/MenuFD2.png"));
		this.FondMenu = this.IcoFond.getImage();
		//Buttons
		JInter CJouer=new JInter(1);
		jouer=new Button("/images/boubou2.png");
		jouer.addMouseListener(CJouer);
		this.add(jouer);
		jouer.setLocation(550, 300);
		music = new Checkbox();
		music = new Checkbox();
		
		// ajout des boutons a la scene
		this.add(music);
		music.setBounds(1000, 600, 80, 30);
		// Interaction de la checkbox
		HandlerClass handler = new HandlerClass();
		music.addItemListener(handler);
		// positionnement des boutons
		
		

	}

	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics g2 = (Graphics2D) g;
		g2.drawImage(this.FondMenu, 0, 0, null);

	}
	
	private void repaint_Jouer() {
		this.remove(jouer);
		int q=0;
		JInter CPlayers=new JInter(2);
		IcoFond = new ImageIcon(getClass().getResource("/images/SepiaBackground.png"));
		this.FondMenu = this.IcoFond.getImage();
		String[] Path= {"/images/2player.png","/images/3player.png","/images/4player.png","/images/5player.png","/images/6player.png"};
		for(int i=0;i<5;i++) {
			Button k=new Button(Path[i],2+i);
		    k.addMouseListener(CPlayers);
			this.add(k);
			k.setLocation( 200+q*150, 300);
			q+=1;
			this.add(k);
			
		}
		this.repaint();
	}
	
	@SuppressWarnings("static-access")
	private void repaint_Players(int x) {
		this.removeAll();
		IcoFond = new ImageIcon(getClass().getResource("/images/Background.png"));
		this.FondMenu = this.IcoFond.getImage();
		music = new Checkbox();
	
		// ajout des boutons a la scene
		this.add(music);
		// Interaction de la checkbox
		HandlerClass handler = new HandlerClass();
		music.addItemListener(handler);
		music.setBounds(1000, 600, 80, 30);
		Audio.StopSound();
		actual="src/music/StarQuest.wav";
		Audio.playSound(actual, true);
		

		if(x>150 & x<250) {GM=new Game_Manager(2,this);}
		else if(x>300 && x<420) {GM=new Game_Manager(3,this);}
		else if(x>430 & x<570) {GM=new Game_Manager(4,this);}
		else if(x>600 & x<750) {GM=new Game_Manager(5,this);}
		else if(x>750 & x<850) {GM=new Game_Manager(6,this);}
		
		//GM=new Game_Manager(6,this);
		ButtonManager.PlaceUnitButtons(this);
		for(int i=0;i<GM.getPM().getPlayerList().size();i++) {
			ButtonManager.PlacePlayerButton(GM.getPM().getPlayerList().get(i), this,GM.getLM());
		}
		
		LabelManager.LabelIni(this);
		LabelManager.LabelPlayer(GM.getPM().getPlayerList().get(0));
		LabelManager.setText2(GM.getPM().getPlayerList().get(0));
		Button EndTurn=new Button("/images/EndTurn.png");
		EndTurn.setLocation(1000,600);
		EndTurn.addMouseListener(new JInter(5));
		this.add(EndTurn);
		
		this.repaint();
		
		
	}
	
	private class JInter implements MouseListener {
		private int id;
		public JInter(int ID) {
			this.id=ID;
		}
		@Override
		public void mouseClicked(MouseEvent arg0) {
			switch(this.id) {
			case 1:
				repaint_Jouer();
				break;
			case 2:
				int x=arg0.getXOnScreen
				();
				repaint_Players(x);
				
		
				
				break;
			case 5:
				if(Game_Manager.GetPlayer().getPhase().equals("Second")) {
					
					Player player=Game_Manager.ChangeTourPlayer();
					LabelManager.setText2(player);
					LabelManager.LabelPlayer(player);
				}
				break;
			default:
				break;
			}
		
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}

	private class HandlerClass implements ItemListener {

		public void itemStateChanged(ItemEvent event) {
			if (music.isSelected()) {
				Audio.playSound(actual, true);
			} else {
				Audio.StopSound();
			}

		}
	}
}

package game;

import Fenetre_jeu.Menu_Pan;
import back.Canons;
import back.Cavaliers;
import back.Soldats;
import map.Land_manager;
import map.Territoires;
import players_manager.Player;
import players_manager.Player_manager;

public class Game_Manager {
	private Game_master Board;

	public static Land_manager getLM() {
		return LM;
	}

	public static Player_manager getPM() {
		return PM;
	}

	private static Land_manager LM;
	private static Player_manager PM;

	public Game_Manager(int nbPlayer, Menu_Pan men) {
		Board = new Game_master(nbPlayer);
		LM = Board.getSol();
		PM = Board.getRhalic();
		PM.getPlayerList().get(0).setPhase("First");
		PM.getPlayerList().get(0).GetRenfort(LM);

	}


	public static Player GetPlayer() {
		Player player=null;
		for(int i=0;i<Game_Manager.getPM().getPlayerList().size();i++) {
			if(Game_Manager.getPM().getPlayerList().get(i).getPhase().equals("First") || 
					Game_Manager.getPM().getPlayerList().get(i).getPhase().equals("Second")) {
				player=Game_Manager.getPM().getPlayerList().get(i);
			}
		}
		
		return player;
	}
	public static Player ChangeTourPlayer() {
		Player player = null;
		for (int i = 0; i < Game_Manager.getPM().getPlayerList().size(); i++) {
			if (Game_Manager.getPM().getPlayerList().get(i).getPhase().equals("Second")) {
				if (i < Game_Manager.getPM().getPlayerList().size() - 1) {
					Game_Manager.getPM().getPlayerList().get(i).setPhase("");
					Game_Manager.getPM().getPlayerList().get(i + 1).setPhase("First");
					player=Game_Manager.getPM().getPlayerList().get(i+1);
					
					
				} else {
					Game_Manager.getPM().getPlayerList().get(i).setPhase("");
					Game_Manager.getPM().getPlayerList().get(0).setPhase("First");
					player=Game_Manager.getPM().getPlayerList().get(0);
				}
			}
		}
		player.GetRenfort(Game_Manager.getLM());
		return player;
	}

	public static Player Victory() {
		for (int i = 0; i < PM.getPlayerList().size(); i++) {
			if (PM.getPlayerList().get(i).getNbCountries(LM) == 42) {
				return PM.getPlayerList().get(i);
			}
		}

		return null;
	}

	public void FirstPhase(Player player, int nb, Territoires country) {
		// Scanner sc = new Scanner(System.in);

		// System.out.println("Tour du joueur " + (player.getId() + 1));

		for (int i = 0; i < player.getArmees(); i++) {
			if (nb <= player.getArmees()) {
				for (int k = 0; k < nb; k++) {
					player.PlaceUnit("Soldat", country);
				}
			}

		}
		for (int i = 0; i < player.getCavaliers(); i++) {
			if (nb <= player.getCavaliers()) {
				for (int k = 0; k < nb; k++) {
					player.PlaceUnit("Cavalier", country);
				}
			}
		}

		for (int i = 0; i < player.getCanons(); i++) {
			if (nb <= player.getCanons()) {
				for (int k = 0; k < nb; k++) {
					player.PlaceUnit("Canon", country);
				}
			}
		}

	}

	public void secondPhase(Player player, Territoires Country_Attack, Territoires Country_Def, int nb_Unites,
			String Unit) {
		if (player.equals(Country_Attack.getOwner())) {
			// LM.AfficherVoisins(LM.getByName(Country_Attack));
			// switch pour choisir l'unit�
		} else {
			switch (Unit) {
			case "Soldat":
				LM.AttackinNb(Country_Attack, Country_Def, new Soldats(), nb_Unites);
				break;
			case "Cavalier":
				LM.AttackinNb(Country_Attack, Country_Def, new Cavaliers(), nb_Unites);
				break;
			case "Canons":
				LM.AttackinNb(Country_Attack, Country_Def, new Canons(), nb_Unites);
				break;
			}
		}

	}

	public void End_Turn(Player player) {
		player.setPhase("");
		// fx reset
	}
}

package game;

import java.util.Random;

import map.Land_manager;

import players_manager.Player_manager;

public class Game_master {
	
	private Land_manager Sol=new Land_manager("Terre"); ;
	private Player_manager Rhalic ;

	
	public Land_manager getSol() {
		return Sol;//earth's god
	}

	public Player_manager getRhalic() {
		return Rhalic;//Human's god
	}

	public Game_master(int nb) {
		Initialize(nb);
	}

	public void Initialize(int PlayerNumber) {
		
		Rhalic = new Player_manager(PlayerNumber);// Human Creation
		Setup_Land_Players(Sol, Rhalic);
		Setup_Armies(Rhalic,Sol);

	}

	protected void Setup_Land_Players(Land_manager LM, Player_manager PM) {
		int ln = PM.getPlayerList().size();// number of players
		int nbLand = LM.getLandList().size();// number of lands=42
		System.out.println(nbLand);
		int limite = (nbLand)/ ln;
		for (int i = 0; i < ln; i++) {
			int compteur = 0;
			while (compteur < limite) {
				int rand = random(nbLand);
				if (LM.getLandList().get(rand).getOwner() == null) {
					LM.getLandList().get(rand).setOwner(PM.getPlayerList().get(i));
					compteur += 1;
				}
			}
		}

		if (nbLand / ln != 0) {
			for (int i = 0; i < nbLand; i++) {
				if (LM.getLandList().get(i).getOwner() == null) {
					int rand = random(ln);
					LM.getLandList().get(i).setOwner(PM.getPlayerList().get(rand));
				}
			}
		}

	}
	
	private void Give_Armies(Player_manager PM, int NbUnits,Land_manager LM) {
		int n = PM.getPlayerList().size();
		for (int i = 0; i < n; i++) {
			int c=PM.getPlayerList().get(i).getNbCountries(LM);
			int arm=PM.getPlayerList().get(i).getArmees();// units waiting for order
			arm+=NbUnits - 1 - c;
			PM.getPlayerList().get(i).setArmees(arm);
		}
	}

	protected void Setup_Armies(Player_manager PM,Land_manager LM) {

		int n = PM.getPlayerList().size();
		switch (n) {
		case 2:
			Give_Armies(PM, 40,LM);
			break;
		case 3:
			Give_Armies(PM, 35,LM);
			break;
		case 4:
			Give_Armies(PM, 30,LM);
			break;
		case 5:
			Give_Armies(PM,  25,LM);
			break;
		case 6:
			Give_Armies(PM, 20,LM);
			break;
		}
	}

	protected int random(int b) {
		Random rand = new Random();
		return rand.nextInt(b);
	}
}
